package org.example.service;

public class CashbackService {

    private final int cashbackPercentage = 1;
    private final int cashbackMaximumAmount = 3000_00;
    private final int minimumSpentForCashback = 100_00;

    public int calculateCashback(int moneySpent) {
        int cashback = 0;
        if (moneySpent < minimumSpentForCashback) {
            return cashback;
        }
        cashback = moneySpent * cashbackPercentage / 100_00 * 100;
        if (cashback > cashbackMaximumAmount) {
            return cashbackMaximumAmount;
        }
        return cashback;
    }
}

