package org.example.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CashbackServiceTest {
    @Test
    void shouldCalculate() {
        CashbackService service = new CashbackService();
        int moneySpent = 399_50;
        int expected = 3_00;
        int actual = service.calculateCashback(moneySpent);
        assertEquals(expected, actual);
    }

    @Test
    void shouldCalculateMaximumCashback() {
        CashbackService service = new CashbackService();
        int moneySpent = 500_000_00;
        int expected = 3000_00;
        int actual = service.calculateCashback(moneySpent);
        assertEquals(expected, actual);
    }

    @Test
    void shouldCalculateZero() {
        CashbackService service = new CashbackService();
        int moneySpent = 99_00;
        int expected = 0;
        int actual = service.calculateCashback(moneySpent);
        assertEquals(expected, actual);
    }
}

